import flask
import flask_login
import auth_app

app = flask.Flask(__name__)
app.secret_key = 'some secret key'
app.register_blueprint(auth_app.bp)
app.config.from_object(auth_app.config)
auth_app.login_manager.init_app(app)


@app.route('/this')
@auth_app.login_required
def this():
    return 'You should have had to login to see this'


app.run()
