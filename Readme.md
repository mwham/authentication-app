# Authentication App

This is a backend based on Flask, Flask-Login, Python-LDAP and [uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/) for
providing an authentication layer on top of R Shiny apps (or any kind of app, really) using Nginx's request auth module.
To use, check out the source code to the server and modify the relevant configs. The `wsgi.py` endpoint should be passed
to uWSGI.

Much of this is based on information and code from:
- https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uswgi-and-nginx-on-ubuntu-18-04
- https://github.com/EdinburghGenomics/Reporting-App
