import string
import random
import logging
import sqlalchemy
from argparse import ArgumentParser
from werkzeug.security import generate_password_hash

pw_chars = string.hexdigits + string.punctuation

meta = sqlalchemy.MetaData()
users = sqlalchemy.Table(
    'users',
    meta,
    sqlalchemy.Column('uid', sqlalchemy.String, primary_key=True),
    sqlalchemy.Column('ldap', sqlalchemy.String),
    sqlalchemy.Column('pwhash', sqlalchemy.String, unique=True),
    sqlalchemy.Column('expired', sqlalchemy.Boolean)
)

stdout = logging.StreamHandler()
stdout.setLevel(logging.INFO)
logger = logging.getLogger('admin')
logger.addHandler(stdout)


def get_engine(user_db):
    engine = sqlalchemy.create_engine('sqlite:///' + user_db)
    meta.create_all(engine)
    return engine


def random_string(nchars):
    return ''.join(random.choice(pw_chars) for _ in range(nchars))


def temp_password():
    temp_pw = random_string(16)
    temp_pw_hash = generate_password_hash(temp_pw)
    return temp_pw, temp_pw_hash


def add_local_user(username, conn):
    temp_pw, temp_hash = temp_password()

    conn.execute(
        sqlalchemy.insert(
            users,
            {'uid': username, 'ldap': None, 'pwhash': temp_hash, 'expired': True}
        )
    )
    logger.info('Added user %s with temp password %s', username, temp_pw)


def reset_local_user(username, conn):
    temp_pw, temp_hash = temp_password()

    result = conn.execute(sqlalchemy.select([users], users.c.uid == username)).fetchone()
    if result['ldap']:
        raise TypeError('Cannot reset password for an LDAP user')

    conn.execute(
        sqlalchemy.update(
            users,
            users.c.uid == username,
            {'pwhash': temp_hash, 'expired': True}
        )
    )
    logger.info('Reset user %s with temp password %s', username, temp_pw)


def import_ldap_user(username, conn):
    conn.execute(
        sqlalchemy.insert(
            users,
            {'uid': username, 'ldap': True, 'pwhash': None, 'expired': False}
        )
    )
    logger.info('Imported %s LDAP user %s', username)


def remove_user(username, conn):
    conn.execute(sqlalchemy.delete(users, users.c.uid == username))
    logger.info('Removed user %s', username)


def expire_user(username, conn):
    conn.execute(sqlalchemy.update(users, users.c.uid == username, {'expired': True}))
    logger.info('Expired user %s', username)


def main(argv=None):
    a = ArgumentParser()
    a.add_argument('user_db', help='User DB to administer')
    a.add_argument('action', choices=('init', 'add', 'import', 'reset', 'reset_key', 'expire', 'remove'))
    a.add_argument('username', nargs='?')
    args = a.parse_args(argv)

    engine = get_engine(args.user_db)
    if args.action == 'init':
        return None

    funcs = {
        'add': add_local_user,
        'reset': reset_local_user,
        'expire': expire_user,
        'import': import_ldap_user,
        'remove': remove_user
    }
    with engine.connect() as conn:
        return funcs[args.action](args.username, conn)


if __name__ == '__main__':
    main()
