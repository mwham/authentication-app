
# SQLAlchemy connection string for the user database
AUTH_DB_CONNECTION_STRING = 'sqlite:///users.sqlite'

# External authentication sources for login credentials
AUTH_LDAP_SOURCES = {}
# AUTH_LDAP_SOURCES = {
#     'some_ldap_server': {
#         'url': 'ldap://some.ldap.server',
#         'bind_string': 'uid={uid},ou=people,dc=dome,dc=ldap,dc=server',
#         'uid_key': 'uid'
#     }
# }
