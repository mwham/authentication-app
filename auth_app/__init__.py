"""
Authentication app, written to provide authentication services to Shiny apps.

When a user visits the root of the app, they are redirected to a username/password login page. Here they can
authenticate against a local SQLite user database:

    uid           ldap    pwhash      expired
    a_local_user  null    8ca758b...  1
    an_ldap_user  ease    null        0

If `ldap` is 0/false, then the account is a local user, with a stored password hash. The given password is hashed
and compared with the stored value. If `expired` is true, then the user is redirected to /change_password - this way,
local users are created with temporary password that they then have to change upon first login. Changing the password
then sets `expired` to false.

If `ldap` is true, then the username and password are checked against the UoE LDAP service. This required the server to
be on the UoE network.

The endpoint /authenticate is a simple endpoint that returns `200 OK` if the client is successfully logged in, and
`401 Unauthorized` if not. This should then be used by Nginx's request auth module to enforce authentication on other
endpoints.

Uses some authentication logic from https://github.com/EdinburghGenomics/Reporting-App - from a previous position.
"""

import logging
import flask
import flask_login
import ldap
import sqlalchemy
from functools import wraps
from urllib.parse import quote, unquote
from flask import request
from werkzeug.security import generate_password_hash, check_password_hash
from auth_app import config

__version__ = '0.2'
bp = flask.Blueprint('auth', __name__, template_folder='templates')
logger = logging.getLogger(__name__)


class AuthHandler(flask_login.LoginManager):
    def __init__(self, app=None, add_context_processor=True):
        super().__init__(app, add_context_processor)
        self.engine = self.ldap_sources = self.meta = self.table = None

    def init_app(self, app, add_context_processor=True):
        super().init_app(app, add_context_processor)

        self.engine = sqlalchemy.create_engine(app.config['AUTH_DB_CONNECTION_STRING'])
        self.ldap_sources = app.config['AUTH_LDAP_SOURCES']
        self.meta = sqlalchemy.MetaData()
        self.meta.reflect(self.engine)
        self.table = self.meta.tables['users']

    def user_exists(self, uid):
        with self.engine.connect() as conn:
            cursor = conn.execute(sqlalchemy.select([sqlalchemy.func.count(self.table.c.uid)], self.table.c.uid == uid))
            result = cursor.fetchone()

        return result[0] == 1

    def get_user_record(self, uid):
        with self.engine.connect() as conn:
            return conn.execute(
                sqlalchemy.select(
                    [self.table.c.ldap, self.table.c.pwhash, self.table.c.expired],
                    self.table.c.uid == uid
                )
            ).fetchone()

    def update_password(self, uid, new_pw):
        with self.engine.connect() as conn:
            conn.execute(
                sqlalchemy.update(
                    self.table,
                    self.table.c.uid == uid,
                    {'pwhash': generate_password_hash(new_pw), 'expired': False}
                )
            )


login_manager = AuthHandler()


class User(flask_login.UserMixin):
    def __init__(self, uid):
        self.uid = uid
        self.ldap, self.pwhash, self.expired = login_manager.get_user_record(self.uid)

    @staticmethod
    def get(uid):
        if login_manager.user_exists(uid):
            return User(uid)

        logger.info('Attempted login with nonexistent user %s', uid)

    def check_credentials(self, pw):
        if self.expired:
            logger.info('Tried to log in expired user %s', self.uid)
            return False

        if self.ldap:
            ldap_cfg = login_manager.ldap_sources[self.ldap]
            connection = ldap.initialize(ldap_cfg['url'])
            try:
                connection.simple_bind_s(ldap_cfg['bind_string'].format(**{ldap_cfg['uid_key']: self.uid}), pw)
                logger.info('Successfully logged in user %s', self.uid)
                return True

            except ldap.INVALID_CREDENTIALS:
                logger.info('LDAP auth attempt failed for user %s', self.uid)
                return False

        else:
            return check_password_hash(self.pwhash, pw)

    def is_active(self):
        return not self.expired

    @property
    def is_authenticated(self):
        if 'remember_token' in request.cookies:
            decoded_token = flask_login.decode_cookie(request.cookies['remember_token'])
            return bool(decoded_token)

        else:
            return False

    def get_id(self):
        return self.uid

    def change_pw(self, old_pw, new_pw):
        if not self.check_credentials(old_pw):
            logger.info('Could not change password for user %s - auth check failed', self.uid)
            return False

        if self.ldap:
            logger.error('Cannot change password for %s LDAP user %s', self.ldap, self.uid)
            return False

        login_manager.update_password(self.uid, new_pw)
        logger.info('Changed password for user %s', self.uid)
        return True

    @staticmethod
    def get_login_token():
        return request.cookies.get('remember_token')


@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)


def login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if request.method in flask_login.config.EXEMPT_METHODS:
            return func(*args, **kwargs)

        elif flask.current_app.config.get('LOGIN_DISABLED'):
            return func(*args, **kwargs)

        elif request.authorization and 'username' in request.authorization:
            u = User.get(request.authorization.username)
            if u and u.check_credentials(request.authorization.password):
                return func(*args, **kwargs)
            else:
                return flask.current_app.login_manager.unauthorized()

        elif not flask_login.current_user.is_authenticated:
            return flask.current_app.login_manager.unauthorized()

        return func(*args, **kwargs)
    return decorated_view


@login_manager.unauthorized_handler
def unauthorised_handler():
    return flask.redirect('/login?redirect=' + quote(flask.request.full_path, safe=()))


@bp.route('/')
@login_required
def main_page():
    return flask.render_template('main_page.html')


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if flask.request.method == 'GET':
        return flask.render_template(
            'login.html',
            message='Log in here',
            redirect=quote(flask.request.args.get('redirect', ''), safe=())
        )

    username = flask.request.form['username']
    redirect = flask.request.form['redirect']
    u = User.get(username)

    if u.check_credentials(flask.request.form['pw']):
        flask_login.login_user(u, remember=True)
        logger.info('Logged in user %s', username)

        if u.expired and u.ldap is False:
            return flask.redirect('/change_password')

        return flask.redirect(unquote(redirect or '/'))

    return flask.render_template(
        'login.html',
        message='Invalid credentials',
        redirect=redirect
    )


@bp.route('/authenticate', methods=['GET', 'POST'])
def authenticate():
    if flask_login.current_user.is_authenticated:
        return 'success', 200
    else:
        return 'unauthorized', 401


@bp.route('/logout')
@flask_login.login_required
def logout():
    logger.info('Logging out user %s', flask_login.current_user.uid)
    flask_login.logout_user()
    return flask.render_template('login.html', message='Logged out')


@bp.route('/change_password', methods=['GET', 'POST'])
@flask_login.login_required
def change_password():
    if flask.request.method == 'GET':
        return flask.render_template('change_password.html')

    elif flask.request.method == 'POST':
        u = User(flask_login.current_user.uid)
        form = flask.request.form
        if u.change_pw(form['old_pw'], form['new_pw']):
            return flask.redirect('/')
        else:
            return flask.render_template('login.html', message='Invalid credentials - could not change password')


@bp.context_processor
def add_auth_version():
    return {'auth_version': __version__}
